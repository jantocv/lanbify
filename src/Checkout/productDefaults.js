const productDefaults = [
  {
    image: '/img/shirt.png',
    name: 'Lana T-Shirt',
    description: 'Description of the product',
    code: 'X7R2OPX',
    priceUnit: 20.0,
    quantity: 0
  },
  {
    image: '/img/mug.png',
    name: 'Lana Mug',
    description: 'Description of the product',
    code: 'X2G2OPZ',
    priceUnit: 7.5,
    quantity: 0
  },
  {
    image: '/img/cap.png',
    name: 'Lana Cap',
    description: 'Description of the product',
    code: 'X3W2OPY',
    priceUnit: 5.0,
    quantity: 0
  }
];

export default productDefaults;
