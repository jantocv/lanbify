export const defaultMockDiscounts = [
  {
    discountId: 'x3-X7R2OPX',
    description: 'x3 Lana T-Shirt offer',
    discount: 0
  },
  {
    discountId: '2x1-X3W2OPY',
    description: '2x1 Lana Cap offer',
    discount: 0
  },
  { discountId: 'promoCode', description: 'Promo Code', discount: 0 }
];

export const mockWithItemsSummary = [
  {
    image: '/img/shirt.png',
    name: 'Lana T-Shirt',
    description: 'Description of the product',
    code: 'X7R2OPX',
    priceUnit: 20.0,
    quantity: 5
  },
  {
    image: '/img/mug.png',
    name: 'Lana Mug',
    description: 'Description of the product',
    code: 'X2G2OPZ',
    priceUnit: 7.5,
    quantity: 4
  },
  {
    image: '/img/cap.png',
    name: 'Lana Cap',
    description: 'Description of the product',
    code: 'X3W2OPY',
    priceUnit: 5.0,
    quantity: 4
  }
];

export const mockWithItemsDiscounts = [
  {
    discountId: 'x3-X7R2OPX',
    description: 'x3 Lana T-Shirt offer',
    discount: 5
  },
  {
    discountId: '2x1-X3W2OPY',
    description: '2x1 Lana Cap offer',
    discount: 10
  },
  { discountId: 'promoCode', description: 'Promo Code', discount: 0 }
];
