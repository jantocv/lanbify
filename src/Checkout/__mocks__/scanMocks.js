export const oneTshirtSummaryMock = [
  {
    image: '/img/shirt.png',
    name: 'Lana T-Shirt',
    description: 'Description of the product',
    code: 'X7R2OPX',
    priceUnit: 20,
    quantity: 1
  },
  {
    image: '/img/mug.png',
    name: 'Lana Mug',
    description: 'Description of the product',
    code: 'X2G2OPZ',
    priceUnit: 7.5,
    quantity: 0
  },
  {
    image: '/img/cap.png',
    name: 'Lana Cap',
    description: 'Description of the product',
    code: 'X3W2OPY',
    priceUnit: 5,
    quantity: 0
  }
];

export const discountWithOneTShirt = [];
export const totalSummaryWithOneTShirt = 20.0;
export const totalPriceWithOneTShirt = 20.0;
